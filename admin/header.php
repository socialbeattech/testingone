<?php
//global $db;
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>ELSS | Dashboard</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" />
<link rel="stylesheet" href="plugins/jQueryUI/jquery-ui-1.10.0.custom.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
<link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" />
<link rel="stylesheet" href="plugins/select2/select2.min.css" />
<link rel="stylesheet" href="plugins/iCheck/all.css" />
<link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css" />
<link rel="stylesheet" href="dist/css/formbuilder-min.css" />
<link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
<link rel="stylesheet" href="dist/css/skins/_all-skins.min.css" />

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript" src="plugins/jQuery/jQuery-1.12.3.min.js"></script>
<script src="//cdn.ckeditor.com/4.5.9/full/ckeditor.js"></script>
<script type="text/javascript">
var $=jQuery.noConflict();
</script>
</head>
<body class="hold-transition skin-blue sidebar-mini sidebar-collapse">
<div class="wrapper">
	<header class="main-header">		
		<a href="<?php echo admin_url(); ?>" class="logo">
			<span class="logo-mini"><b>TM</b> Dash</span>
			<span class="logo-lg"><b>Tata Mutuals</b></span>
		</a>
		
		<nav class="navbar navbar-static-top">			
			<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
				<span class="sr-only">Toggle navigation</span>
			</a>
			
			<div class="navbar-custom-menu">
			</div>
		</nav>
	</header>
	
	<aside class="main-sidebar">
		<section class="sidebar">
			<ul class="sidebar-menu">
				<li class="header">MAIN NAVIGATION</li>
				<li class="active">
					<a href="<?php echo admin_url(); ?>">
						<i class="fa fa-dashboard"></i> <span>Form Entries</span>						
					</a>
				</li>
			</ul>
		</section>
	</aside>
	
	<div class="content-wrapper">
		<section class="content-header">
			<h1>Form Entries</h1>
			<ol class="breadcrumb">
				<li><a href="<?php echo admin_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Form Entries</li>
			</ol>
		</section>
<?php
include('functions.php');
global $db;
session_start();
if (!(isset($_SESSION['login']) && $_SESSION['login'] != ''))
{
	header ("Location: login.php");
}
get_header();
?>
<?php
$query = "SELECT * FROM `".TABLE_PREFIX."entries`";
$count = $db->getCount($query);
$entries = $db->getRows($query);
?>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">List of Forms</h3>
				</div>
				
				<div class="box-body">
					<table id="forms" class="table table-bordered table-striped">
						<thead>
							<?php $fields = unserialize($entry['entries'][0]); ?>
							<tr>
								<th>S.No</th>
								<th>Name</th>
								<th>Email</th>
								<th>Mobile</th>
								<th>Last Entry</th>
							</tr>
						</thead>
						
						<tbody>
							<?php							
							$i=1;							
							foreach($entries as $entry)
							{
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<?php $fields = unserialize($entry['entry']); ?>
								<td><?php print_r($fields); ?></td>
								<?php
								foreach($fields as $field)
								{
								?>
								<td><?php echo $field['name']; ?></td>
								<?php } ?>
							</tr>
							<?php $i++; } ?>
						</tbody>
						
						<tfoot>
							<tr>
								<th>S.No</th>
								<th>Form Name</th>
								<th>Script Tag</th>
								<th>Entries</th>
								<th>Last Entry</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if($count){ ?>
<script>
	jQuery(document).ready(function($){
		$("#forms").DataTable();
	});
</script>
<?php } ?>
<?php get_footer(); ?>
<?php
//global $db;
?>
	</div>

	<footer class="main-footer">
		<strong>Copyright &copy; <?php echo date('Y'); ?> <a href="http://www.socialbeat.in/" target="_blank">Social Beat</a>.</strong> All rights
		reserved.
	</footer>

</div>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="plugins/fastclick/fastclick.js"></script>
<script type="text/javascript" src="plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="plugins/select2/select2.full.min.js"></script>
<script type="text/javascript" src="plugins/sparkline/jquery.sparkline.min.js"></script>
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script type="text/javascript" src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script type="text/javascript" src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script type="text/javascript" src="plugins/chartjs/Chart.min.js"></script>
<script type="text/javascript" src="plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="dist/js/app.min.js"></script>
<script type="text/javascript" src="vendor/js/vendor.js"></script>
<script type="text/javascript" src="dist/js/formbuilder-min.js"></script>
<script type="text/javascript" src="dist/js/demo.js"></script>
</body>
</html>
<?php
include('functions.php');
global $db;
if(isset($_POST['submit']))
{
	$postedvalueserialized = serialize($_POST);
	$postedvaluearray = unserialize($postedvalueserialized);
	$db->insertRow("INSERT INTO `".TABLE_PREFIX."form_entries` (`form_id`,`client_id`,`entry`) VALUES ('".$_POST['form-id']."','".$_POST['client-id']."','".serialize($_POST)."')");
	$notifications = $db->getRows("SELECT * FROM `".TABLE_PREFIX."notifications` WHERE `form_id` = '".$_POST['form-id']."' AND `client_id` = '".$_POST['client-id']."' AND `status` = '1'");
	foreach($notifications as $notification)
	{
		$message = '<html><body>';
		if(strip_tags($notification['message'])=='{all_fields}')
		{
			$message .= '<table rules="all" style="border-color: #666;" cellpadding="10">';
			$message .= "<tr style='background: #eee;text-align:left;'><th><strong>Field Name</strong> </th><th>Value</th></tr>";
			foreach($postedvaluearray as $postedkey=>$postedvalue)
			{
				if($postedkey!='form-id' || $postedkey!='client-id' || $postedkey!='confirmation-url' || $postedkey!='submit')
				{
					if($postedkey=='form-url')
					{
						$message .= "<tr><td><strong>Form Submitted Url</strong> </td><td>" . $postedvalue . "</td></tr>";
					}
					else
					{
						$message .= "<tr><td><strong>".ucwords(str_replace("-", " ",$postedkey))."</strong> </td><td>" . $postedvalue . "</td></tr>";
					}
				}
			}
			$message .= "</table>";
		}
		$message .= "</body></html>";
		$headers = "From: " . $notification['from_name']. "<".$notification['from_email'].">" . "\r\n";
		if($notification['reply_to'])
		{
			$headers .= "Reply-To: ". $notification['reply_to'] . "\r\n";
		}
		if($notification['cc_email'])
		{
			$headers .= "CC: ". $notification['cc_email'] . "\r\n";
		}
		if($notification['bcc_email'])
		{
			$headers .= "BCC: ". $notification['bcc_email'] . "\r\n";
		}
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		mail($notification['to_email'],$notification['subject'],$message,$headers);
	}
}
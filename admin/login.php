<?php
include('functions.php');
global $db;
session_start();
if(isset($_SESSION['login']))
{
	header ("Location: index.php");
}
$error = '';
if(isset($_POST['login']))
{
	$query = "SELECT * FROM `".TABLE_PREFIX."users` WHERE `email` = '".$_POST['email']."' AND `password` = '".md5($_POST['password'])."'";
	$countuser = $db->getCount($query);	
	if($countuser == 1)
	{
		$loggeduser = $db->getRow($query);
		$_SESSION['login'] = $loggeduser['id'];
		header ("Location: index.php");
	}
	else
	{
		$error = '<p class="text-danger bg-danger text-center">Email/Password doesn\'t match</p>';
	}
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Tata Mutuals | Log in</title>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="plugins/iCheck/square/blue.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
	<div class="login-logo">
		<a href="<?php echo admin_url(); ?>"><b>Tata Mutuals</b></a>
	</div>
  
	<div class="login-box-body">
		<p class="login-box-msg">Sign in to start your session</p>

		<form method="post" action="<?php echo admin_url(); ?>/login.php">
			<div class="form-group has-feedback">
				<input type="text" name="email" class="form-control" placeholder="User Name">
				<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
			</div>
			<div class="form-group has-feedback">
				<input type="password" name="password" class="form-control" placeholder="Password">
				<span class="glyphicon glyphicon-lock form-control-feedback"></span>
			</div>
			<div class="row">
				<div class="col-xs-8">
					<div class="checkbox icheck">
						<label>
							<input type="checkbox"> Remember Me
						</label>
					</div>
				</div>
				
				<div class="col-xs-4">
					<button type="submit" class="btn btn-primary btn-block btn-flat" name="login">Sign In</button>
				</div>
			</div>
		</form>
		
		<?php echo $error; ?>
	</div>
</div>

<script src="plugins/jQuery/jQuery-2.2.0.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>

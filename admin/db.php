<?php
include('config.php');
Class CRM_DB
{	
	public $isConnected;
	protected $datab;
	public function __construct($username, $password, $host, $dbname, $options=array())
	{
		$this->isConnected = true;
		try { 
			$this->datab = new PDO("mysql:host={$host};dbname={$dbname};charset=utf8", $username, $password, $options); 
			$this->datab->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); 
			$this->datab->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
		} 
		catch(PDOException $e) { 
			$this->isConnected = false;
			throw new Exception($e->getMessage());
		}
	}
	public function Disconnect()
	{
		$this->datab = null;
		$this->isConnected = false;
	}
	
	public function getRow($query)
	{
		try{
			return $this->datab->query($query)->fetch();  
		}catch(PDOException $e){
			throw new Exception($e->getMessage());
		}
	}
	
	public function getCount($query)
	{
		try{
			return $this->datab->query($query)->fetchColumn();  
		}catch(PDOException $e){
			throw new Exception($e->getMessage());
		}
	}
	
	public function getRows($query)
	{
		try{
			return $this->datab->query($query)->fetchAll();       
		}catch(PDOException $e){
			throw new Exception($e->getMessage());
		}
	}
	
	public function insertRow($query)
	{
		try{ 
			return $this->datab->query($query);
		}catch(PDOException $e){
			throw new Exception($e->getMessage());
		}
	}
	public function updateRow($query)
	{
		return $this->insertRow($query);
	}
	public function deleteRow($query)
	{
		return $this->insertRow($query);
	}
}
global $db;
$db = new CRM_DB(DB_USER, DB_PASSWORD, DB_HOST, DB_NAME, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
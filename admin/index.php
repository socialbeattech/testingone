<?php
include('functions.php');
global $db;
session_start();
if (!(isset($_SESSION['login']) && $_SESSION['login'] != ''))
{
	header ("Location: login.php");
}
get_header();
$query = "SELECT * FROM `".TABLE_PREFIX."entries`";
$count = $db->getCount($query);
$entries = $db->getRows($query);
?>
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">List of entries</h3>
				</div>
				
				<div class="box-body table-responsive">
					<table id="forms" class="table table-bordered table-striped">
						<thead>
							<?php $fields = json_decode($entries[0]['entry']); ?>
							<tr>
								<th>S.No</th>
								<?php
								foreach($fields as $key =>$field)
								{
									if($key!='UserMessage')
									{
								?>
								<th><?php echo $key; ?></th>
								<?php } ?>
								<?php } ?>
								<th>Entry Date/Time</th>
							</tr>
						</thead>
						
						<tbody>
							<?php							
							$i=1;							
							foreach($entries as $entry)
							{
							?>
							<tr>
								<td><?php echo $i; ?></td>
								<?php $fields = json_decode($entry['entry']); ?>
								<?php
								foreach($fields as $key =>$field)
								{
									if($key!='UserMessage')
									{
								?>
								<td><?php echo $field; ?></td>
								<?php } ?>
								<?php } ?>
								<td><?php echo $entry['time'] ?></td>
							</tr>
							<?php $i++; } ?>
						</tbody>
						
						<tfoot>
							<?php $fields = json_decode($entries[0]['entry']); ?>
							<tr>
								<th>S.No</th>
								<?php
								foreach($fields as $key =>$field)
								{
									if($key!='UserMessage')
									{
								?>
								<th><?php echo $key; ?></th>
								<?php } ?>
								<?php } ?>
								<th>Entry Date/Time</th>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js"></script>
<script>
	jQuery(document).ready(function($){
		$("#forms").DataTable({
			dom: 'lBfrtip',
            buttons: ['csv','pdf']
		});
	});
</script>
<style>
.dt-button {
    padding: 05px 15px;
    background: #3c8dbc;
    color: #fff;
    margin-right: 5px;
    margin-top: 15px;
    display: inline-block;
	cursor:pointer;
}
</style>
<?php get_footer(); ?>
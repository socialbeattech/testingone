<?php
include_once('db.php');
function get_header()
{
	global $db;
	include('header.php');
}

function get_footer()
{
	global $db;
	include('footer.php');
}

function admin_url()
{
	global $db;
	$site_url  = $db->getRow("SELECT `value` FROM `".TABLE_PREFIX."options` WHERE `id` = 1");
	return $site_url['value'];
}

function get_json($id)
{
	global $db;
	$json  = $db->getRow("SELECT `json` FROM `".TABLE_PREFIX."forms` WHERE `form_id` = '".$id."'");
	return $json['json'];
}

function getAdminEmail($id)
{
	global $db;
	$email  = $db->getRow("SELECT `email` FROM `".TABLE_PREFIX."users` WHERE `id` = '".$id."'");
	return $email['email'];
}

function getFormName($id)
{
	global $db;
	$name  = $db->getRow("SELECT `name` FROM `".TABLE_PREFIX."forms` WHERE `form_id` = '".$id."'");
	return $name['name'];
}